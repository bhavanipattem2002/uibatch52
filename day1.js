console.log("hello world");
//var,let,const

if(true){
    var a=20;
    console.log(a);
}
console.log(a);

//Data Types
let name = "javascript";
let id = 101;
let coursecompletion = true;  
let job = null;
let batch;
console.log("Hrllo");
console.log(typeof(name));
console.log(typeof(id));
console.log(typeof(coursecompletion));
console.log(typeof(job));
console.log(typeof(batch));

//complex datatypes
let scores=[34,"hello",98,99,72];
console.log(scores[1]);
console.log(scores[0]);
console.log(scores[2]);
console.log(scores.length);

//looping statements for loop,while loop,do while
for(let i=0;i<scores.length;i++){
    console.log(scores[i]);
}

//fuction--block of reusable code

function addition(){    
    let result=30+40;
    return result;
}//function declaration

console.log(addition()) //fuction calling

function subtraction(num1,num2){
    return num1-num2
}

console.log(subtraction(50,10));

//let firstnum=prompt("enter the value");
//console.log(firstnum);


//let firstnumber=prompt("enter the value");
//alert("the value you have entered is "+firstnumber);
//console.log(firstnum);


//create a function that should accept two values from the user and print the max value
function findmax(num1,num2){
    return Math.max(num1,num2);
    
}
console.log(findmax(20,90));

//find a min value between three values
function findmin(num1,num2,num3){
    return Math.min(num1,num2,num3);
}
console.log(findmin(200,30,40));

//find a random number btw 0 and 1
let randomnumber=Math.random( )
console.log(randomnumber);

//find randomnumber btw 0 and 1
let randmnumber=Math.round(Math.random()*100);

console.log(randmnumber);

console.log(Math.ceil(56.98));
console.log(Math.abs(45));
console.log(Math.pow(2,5));
 
//find even number,odd number between 1 to 100
//print even numbers and odd numbers between 1 to 50

//even number from 1 to 100

for(i=1;i<100;i++)
{
    if(i%2==0)
    console.log(i);

}
//odd number from 1 to 100

for(i=1;i<100;i++)
{
    if(i%2!=0)
    console.log(i);

}
//even numbers from 1 to 50

for(i=1;i<50;i++)
{
    if(i%2==0) 
    console.log(i);

}
//odd numbers from 1 to 50

for(i=1;i<50;i++)
{
    if(i%2!=0)
    console.log(i);

}